---
title: 植物と動物
date: 2017-04-07 06:32:39
tags: [wanikani, level_12]
---
単語：
* 動く　　　　　ー　うごく　　　　　ー　to move
* 行動　　　　　ー　こうどう　　　　ー　action
* 活動　　　　　ー　かつどう　　　　ー　action
* 植える　　　　ー　うえる　　　　　ー　to plant
* 植物　　　　　ー　しょくぶつ　　　ー　plant (vegetation)
* 泉　　　　　　ー　いずみ　　　　　ー　spring
* 運動　　　　　ー　うんどう　　　　ー　exercise
* 動物　　　　　ー　どうぶつ　　　　ー　animal

漢字：
* 悲　　　　　　ー　かな　　　　　　ー　sad

