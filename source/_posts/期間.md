---
title: 期間
date: 2017-04-12 08:35:29
tags: [wanikani, level_12]
---
単語：
* 都会　　　　ー　とかい　　　　　ー　city
* 本流　　　　ー　ほんりゅう　　　ー　mainstream
* 時期　　　　ー　じき　　　　　　ー　time / season
* 短期　　　　ー　たんき　　　　　ー　short period
* 全員　　　　ー　ぜんいん　　　　ー　all members
* 期間　　　　ー　きかん　　　　　ー　period
* 

問題のある言葉：
* 欠ける　(to lack) 🆚　欠かす (to miss)


