---
title: 虫歯
date: 2017-04-05 07:45:44
tags: [wanikani, level_12]
---
単語：
* 短刀　　　　ー　たんとう　　　　ー　short sword
* 虫歯　　　　ー　むしば　　　　　ー　cavity
* 茶の湯　　　ー　ちゃのゆ　　　　ー　tea ceremony
* 地球　　　　ー　ちきゅう　　　　ー　the earth
* 根本　　　　ー　こんぽん　　　　ー　root (source)
* 根　　　　　ー　ね　　　　　　　ー　root
* 屋根　　　　ー　やね　　　　　　ー　roof
* 湯気　　　　ー　ゆげ　　　　　　ー　steam 
* 根気　　　　ー　こんき　　　　　ー　patience
* 大根　　　　ー　だいこん　　　　ー　daikon


漢字：
* 野　　　　　ー　や　　　　　　　ー　field
* 悪　　　　　ー　あく　　　　　　ー　bad

問題のある言葉：
* 場　　　　　ー　ば　　　　　　　ー　location
* 湯　　　　　ー　ゆ　　　　　　　ー　hot water
* 陽　　　　　ー　よう　　　　　　ー　sunshine

