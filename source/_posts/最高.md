---
title: 最高
date: 2017-02-28 08:29:23
tags: [wanikani, level_10]
---
新しい言葉：
* 鉄人　　　ー　てつじん　　　　　ー　ironman
* 進む　　　ー　すすむ　　　　　　ー　to advance
* 最高　　　ー　さいこう　　　　　ー　the best
* 親　　　　ー　おや　　　　　　　ー　parent
* 病　　　　ー　びょう　　　　　　ー　sick
* 終点　　　ー　しゅうてん　　　　ー　last stop
* 集める　　ー　あつめる　　　　　ー　to collect
* 集金　　　ー　しゅうきん　　　　ー　collecting money
* 最近　　　ー　さいきん　　　　　ー　recent

問題のある言葉：
* 当たり前　　　　ー　natural
* 見直す　　　　　ー　to reevaluate
* 発見　　　　　　ー　discovery
* 行き　　　　　　ー　train direction

