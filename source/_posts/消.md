---
title: 消
date: 2017-04-03 10:29:08
tags: [wanikani, level_12]
---
漢字：
* 消　　　　ー　しょう　　　　ー　extinguish
* 動　　　　ー　どう　　　　　ー　move
* 員　　　　ー　いん　　　　　ー　member
* 温　　　　ー　おん　　　　　ー　warm
* 商　　　　ー　しょう　　　　ー　merchandise
* 植　　　　ー　しょく　　　　ー　plant

単語：
* 労働　　　ー　ろうどう　　　ー　manual labour
* 落ち　　　ー　おち　　　　　ー　conclusion
* 大学院生　ー　　　　　　　　ー　graduate student
* 集まる　　ー　あつまる　　　ー　to come together
* 神道　　　ー　しんとう　　　ー　shinto
* 命令　　　ー　めいれい　　　ー　order
* 終電　　　ー　しゅうでん　　ー　last train
