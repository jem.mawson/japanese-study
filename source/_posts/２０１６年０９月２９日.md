---
title: ２０１６年０９月２９日
date: 2016-09-29 08:52:20
tags: [wanikani, level_8]
---
新しい言葉：
* クジラ
* 民（みん）
* 船（せん）
* 由（ゆう）
* 家（か）
* 天国（てんごく）
* 首になる（くびになる）

I reviewed:
* し - providing multiple reason
* そうです - saying something seems a particular way
* てみたい - want to try

I realised I don't know what ので does
