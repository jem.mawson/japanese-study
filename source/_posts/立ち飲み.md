---
title: 立ち飲み
date: 2017-03-02 08:02:26
tags: [wanikani, level_10]
---
新しい言葉：
* 新聞　　　　　ー　しんぶん
* 心配　　　　　ー　しんぱい　　　　　ー　worry
* 天使　　　　　ー　てんし　　　　　　ー　angel
* 習う　　　　　ー　ならう
* 立ち飲み　　　ー　たちのみ　　　　　ー　stand and drink
* 顔付き　　　　ー　かおつき　　　　　ー　facial expression
* 配る　　　　　ー　くばる　　　　　　ー　to distribute

問題のある言葉：
* 大作　　　　　ー　epic
* 当たり　　　　ー　a success
* 大気　　　　　ー　atmosphere
* 向　　　　　　ー　こう　　　　　　ー　yonder
* 開発　　　　　ー　かいはつ　　　　ー　development
* 当たり前　　　ー　natural
